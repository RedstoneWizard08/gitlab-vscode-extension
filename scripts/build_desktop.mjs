import { buildDesktop } from './utils/jobs.mjs';

async function main() {
  await buildDesktop();
}

main();
